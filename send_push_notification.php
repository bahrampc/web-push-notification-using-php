<?php
require 'vendor/autoload.php';
use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;

// here I'll get the subscription endpoint in the POST parameters
// but in reality, you'll get this information in your database
// because you already stored it (cf. push_subscription.php)
$request_data=json_decode(file_get_contents('php://input'), true);
// print_r($request_data);exit;
$subscription = Subscription::create($request_data);

$auth = array(
    'VAPID' => array(
        'subject' => 'https://github.com/Minishlink/web-push-php-example/',
        'publicKey' => 'BGhZPy2ET7e27b0Ln1YLAZ_oryxFdRNTbEa3aRdByAlUIkhzrCCrXiiVFE7MWjrSB7RcceR41wOBP9DAPciF8Xc', // don't forget that your public key also lives in app.js
        'privateKey' => 'hIcS1YWPil1uMRRWVF7HyyRPfTKpoZiwUhHppHxF9iY', // in the real world, this would be in a secret file
    ),
);
// print_r($subscription);exit;
$webPush = new WebPush($auth);
$notification_data['title']=($request_data['body_data']['title'])?$request_data['body_data']['title']:'This is title';
$notification_data['body']=($request_data['body_data']['body'])?$request_data['body_data']['body']:'This is body';
$notification_data['url']=($request_data['body_data']['url'])?$request_data['body_data']['url']:'https://www.itinfotech.in/';
$notification_data['icon']='images/icon.png';
$notification_data['badge']='images/badge.png';

$res = $webPush->sendNotification(
    $subscription,
    json_encode($notification_data)
);

// handle eventual errors here, and remove the subscription from your server if it is expired
foreach ($webPush->flush() as $report) {
    $endpoint = $report->getRequest()->getUri()->__toString();

    if ($report->isSuccess()) {
        $response_data['code']=1;
        $response_data['msg']="Notification sent successfully for subscription {$endpoint}";
        echo json_encode($response_data);exit;
    } else {
        $response_data['code']=0;
        $response_data['msg']="[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
        echo json_encode($response_data);exit;
       
    }
}