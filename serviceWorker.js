let notification_data = false;
self.addEventListener('push', function(event) {
    notification_data = JSON.parse(event.data.text());

    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }
    if (event.data) {

        const title = notification_data.title;
        const options = {
            body: notification_data.body,
            icon: notification_data.icon,
            badge: notification_data.badge
        };
        event.waitUntil(self.registration.showNotification(title, options));
    }
});

self.addEventListener('notificationclick', function(event) {
    console.log('[Service Worker] Notification click Received.');
    console.log(notification_data);
    // var notification_data = JSON.parse(event.data.text());
    event.notification.close();

    event.waitUntil(
        clients.openWindow(notification_data.url)
    );
});